<?php 
include('./config.php');

if (isset($_POST['conf1rm']) && $_POST['conf1rm'] == "4cc3pt3d!"){
	
	if (!isset($_SESSION)) {
		session_start();
	}
	
	$loginFormAction = $_SERVER['PHP_SELF'];
	if (isset($_GET['accesscheck'])) {
		$_SESSION['PrevUrl'] = $_GET['accesscheck'];
	}
	if (isset($_POST['user'])) {
		$loginUsername=strip_tags($_POST['user']);
		$passWord=$_POST['pass'];
		$passWord=md5($passWord);
		$MM_fldUserAuthorization = "level";
		$MM_redirectLoginSuccess = "./index.php";
		$MM_redirectLoginFailed = "?error=5682592762";
		$MM_redirecttoReferrer = false;
		mysql_select_db($database_connect, $conn);
		
		$LoginRS__query=sprintf("SELECT usr, pwd, level, fullname FROM tbl_user WHERE usr='%s' AND pwd='%s'",
		get_magic_quotes_gpc() ? $loginUsername : addslashes($loginUsername), get_magic_quotes_gpc() ? $passWord : addslashes($passWord)); 
	   
		$LoginRS = mysql_query($LoginRS__query, $conn) or die(mysql_error());
		$loginFoundUser = mysql_num_rows($LoginRS);
		if ($loginFoundUser) {

			$loginStrGroup  = mysql_result($LoginRS,0,'level');
			$fullname  = mysql_result($LoginRS,0,'fullname');
			
			$_SESSION['MM_Username'] = $loginUsername;
			$_SESSION['MM_UserGroup'] = $loginStrGroup;
			$_SESSION['MM_UserPass'] = $passWord;
			$_SESSION['MM_fullname'] = $fullname;

			if (isset($_SESSION['PrevUrl']) && false) {
				$MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
			}
			header("Location: " . $MM_redirectLoginSuccess );
		}
		else {
		header("Location: ". $MM_redirectLoginFailed );
		}
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration Login</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

        <!-- Reset all CSS rule -->
        <link rel="stylesheet" href="css/reset.css" />
        
        <!-- Main stylesheed  (EDIT THIS ONE) -->
        <link rel="stylesheet" href="css/style.css" />

        
        <!-- jQuery AND jQueryUI -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="css/jqueryui/jqueryui.css" />
        
       
    </head>

    <body>
                
            <div id="content" class="login">
                
                <h1><img src="img/icons/locked.png" alt="" />Admin Login</h1>
                
                <?php
				if ($_GET['error'] == 5682592762) {
				?>
                <div class="notif tip">
                    <?php echo "Login Failed!<br/>Please check username & password"; ?>
                    <a href="#" class="close"></a>
                </div>
				<?php } ?>
                <form action="<?php echo $loginFormAction; ?>" method="post" name="logsys">
				<br/><br/>
				<div class="loginfix">username</div>
                <div class="input placeholder">
                    <label for="login"> </label>
                    <input type="text" name="user" id="login"/>
                </div>
				<div class="loginfix">password</div>
                <div class="input placeholder">
                    <label for="pass"> </label>
                    <input type="password" name="pass" id="pass" value=""/>
					<input type="hidden" name="conf1rm" value="4cc3pt3d!" />
                </div>

                <div class="submit">
                    <input type="submit" name="submit" value="login" /> <input type="reset" class="white" name="reset" value="reset" />
                </div>
                </form>

                
            </div>
        
        
    </body>
</html>
