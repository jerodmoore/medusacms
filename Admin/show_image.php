<?php require_once('Connections/testConn.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_getImage = "-1";
if (isset($_GET['image_id'])) {
  $colname_getImage = $_GET['image_id'];
}
mysql_select_db($database_testConn, $testConn);
$query_getImage = sprintf("SELECT type, content FROM tbl_upload WHERE image_id = %s", GetSQLValueString($colname_getImage, "int"));
$getImage = mysql_query($query_getImage, $testConn) or die(mysql_error());
$row_getImage = mysql_fetch_assoc($getImage);
$totalRows_getImage = mysql_num_rows($getImage);
header('Content-type: ' . $row_getImage['type']);
echo $row_getImage['content'];
mysql_free_result($getImage);
?>
