<?php 
include("config.php.conf");
include("function.php");
$cid=443;
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Web Design Austin TX</title>
	<meta name="description" content="<?php get_meta_desc($cid); ?>" /> 
	<meta name="keywords" content="<?php get_meta_key($cid); ?>" /> 
	
	<!-- FLUID 960 -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
	
	<!--STYLE -->
	<link rel="stylesheet" type="text/css" href="./style.css" />

	<!-- JQUERY -->
	<script type="text/javascript" src="./js/jquery-1.4.3.min.js"></script>
	
	<!-- FANCYBOX -->
	<script type="text/javascript" src="./js/fancybox/jquery.fancybox-1.3.4.js"></script>
	<link rel="stylesheet" href="./js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		
	<script type="text/javascript" >
	$(document).ready(function() {
		$("a.fancylink").fancybox({
			'transitionIn'	:	'fade',
			'transitionOut'	:	'fade',
			'speedIn'		:	300, 
			'speedOut'		:	300,
			'titlePosition' : 'inside',
			'titleShow' : true,
			'overlayColor' : '#000',
			'type' : 'image',	
			'hideOnContentClick': true,
			'onStart': function() {
     				 $('#fancybox-outer').css({'background':'transparent'});
     				 $('#fancybox-bg-n,#fancybox-bg-ne,#fancybox-bg-e,#fancybox-bg-se,#fancybox-bg-s,#fancybox-bg-sw,#fancybox-bg-w,#fancybox-bg-nw')
   				   .css({'background-image':'none'});
				 $('#fancybox-content').css({'border-width':'0 !Important'});
 			   }
		});
	});
	</script>	
</head>

<body>	

	
	<!-- TOP -->
	<?php include("./navigation.php"); ?>
	
	<!-- CONTENT -->
	<div id="container_all">
		<div id="content">
		<div class="container_16">
		
		<!-- CONTENT LEFT -->
		<div class="grid_10"><div id="content_left">
			<img src="show_image.php?id=5" alt="" />
		</div></div>
		
		<!-- CONTENT RIGHT (Text) -->
		<div class="grid_6">
		<div id="content_right">
			<h1>Introduction</h1>
			<?php get_content(443); ?>
			<p><a href="./services.php" title="more">more(...)</a></p>
			<br/><br/><br/>
			
		</div>
		</div>
		<div class="clear">&nbsp;</div>
		</div>
		</div>
	</div>
	
	
	<!-- SUBCONTENT -->
	<?php include("./subcontent.php"); ?>
	
	<!-- FOOTER -->
	<?php include("./footer.php"); ?>
</body>
</html>