<?php 
include("config.php.conf");
?>
<div id="container_header">
	<div class="container_fit">
		<div class="container_16">
			<!-- HEADER -->
			<div id="header">
			<div class="grid_8" id="header_logo"><a href="./" title="Web Design Austin TX"><img src="show_image.php?id=4" alt="Web Design Austin TX" /></a></div>
			<div class="grid_8" id="header_info">
				<div id="header_lang">
					<a href="http://www.facebook.com/lakewaywebdesign" title="Facebook"><img src="./img/icon_fb.png" alt="Facebook"/></a>
					<a href="http://twitter.com/#!/lakewayweb" title="Twitter"><img src="./img/icon_tw.png" alt="Twitter"/></a>
				</div>
				<div id="header_call"><?php get_content(204); ?></div>
			</div>
			<div class="clear">&nbsp;</div>
			</div>
			<!-- MENU -->
			<div id="menu">
			<div class="grid_16">
				<a href="./index.php" title="HOME">HOME</a>
				<a href="./cms.php" title="CMS">CMS</a>
				<a href="./services.php" title="SERVICES">SERVICES</a>
				<a href="./contact.php" title="CONTACT US">CONTACT US</a>
			</div>
			<div class="clear"></div>
			</div>
		</div>
	</div>
	</div>