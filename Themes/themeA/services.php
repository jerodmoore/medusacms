<?php 
include("config.php.conf");
include("function.php");
$cid=199;
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Web Design Austin TX</title>
	<meta name="description" content="<?php get_meta_desc($cid); ?>" /> 
	<meta name="keywords" content="<?php get_meta_key($cid); ?>" /> 
	
	<!-- FLUID 960 -->
	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />
	
	<!--STYLE -->
	<link rel="stylesheet" type="text/css" href="./style.css" />

	<!-- JQUERY -->
	<script type="text/javascript" src="./js/jquery-1.4.3.min.js"></script>
	
	<!-- FANCYBOX -->
	<script type="text/javascript" src="./js/fancybox/jquery.fancybox-1.3.4.js"></script>
	<link rel="stylesheet" href="./js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
		
	<script type="text/javascript" >
	$(document).ready(function() {
		$("a.fancylink").fancybox({
			'transitionIn'	:	'fade',
			'transitionOut'	:	'fade',
			'speedIn'		:	300, 
			'speedOut'		:	300,
			'titlePosition' : 'inside',
			'titleShow' : true,
			'overlayColor' : '#000',
			'hideOnContentClick': true
		});
	});
	</script>	
</head>

<body>	
	<!-- TOP -->
	<?php include("./navigation.php"); ?>
	
	<!-- SERVICE -->
	<div id="container_all">
		<div id="service">
		<div class="container_16">
		
			<!-- SERVICE LEFT -->
			<div class="grid_6">
				<img src="show_image.php?id=6" alt="" id="service_left"/>
			</div>
			
			<!-- SERVICE CENTER -->
			<div class="grid_10">
			<div id="service_content">
				<?php get_content(199); ?> 
				
			</div>
			</div>
			

			<div class="clear">&nbsp;</div>
		</div>
		</div>
	</div>
	
	
	<!-- SUBCONTENT -->
	<?php include("./subcontent.php"); ?>
	
	<!-- FOOTER -->
	<?php include("./footer.php"); ?>
</body>
</html>