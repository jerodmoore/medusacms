<?php 

	include("./config.php.conf"); 

	include("./function.php"); 

	$m=$_GET[m];

?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<title>San Antonio Web Design</title>

	<meta name="description" content="San Antonio Web Desgin Official Website" /> 

	<meta name="keywords" content="san antonio, web design" /> 

	

	<!-- FLUID 960 -->

	<link rel="stylesheet" type="text/css" href="css/reset.css" media="screen" />

	<link rel="stylesheet" type="text/css" href="css/grid.css" media="screen" />

	

	<!--STYLE -->

	<link rel="stylesheet" type="text/css" href="./style.css" />



	<!-- JQUERY -->

	<script type="text/javascript" src="./js/jquery-1.4.3.min.js"></script>

	

	<!-- FANCYBOX -->

	<script type="text/javascript" src="./js/fancybox/jquery.fancybox-1.3.4.js"></script>

	<link rel="stylesheet" href="./js/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />

		

	<script type="text/javascript">

	$(document).ready(function() {

		$("a.fancylink").fancybox({

			'transitionIn'	:	'fade',

			'transitionOut'	:	'fade',

			'speedIn'		:	300, 

			'speedOut'		:	300,

			'titlePosition' : 'inside',

			'titleShow' : true,

			'overlayColor' : '#000',

			'hideOnContentClick': true

		});

	});

	</script>	

</head>



<body>

<!-- HEADER -->

<div class="container_fit">

<div class="container_16">

	<div class="grid_3" id="header_logo"><a href="./index.html" title="San Antonio Website Design"><img src="./img/logo.png" alt="San Antonio Website Design" /></a></div>

	<div class="grid_11" id="header_menu">

	<!-- MENU -->

		<a href="./" title="HOME">HOME</a>

		<a href="./?m=02" title="ABOUT US">ABOUT US</a>

		<a href="./?m=02" title="SERVICES">SERVICES</a>

		<a href="./portfolio" title="PORTFOLIO">PORTFOLIO</a>

		<a href="./?m=04" title="CONTACT US">CONTACT US</a>

	</div>

	<div class="grid_2" id="header_social">

		<a href="#" title="join my fan page"><img src="./img/icon_fb.png" alt="facebook" /></a> 

		<a href="#" title="follow my twitter"><img src="./img/icon_tw.png" alt="twitter" /></a>

		<a href="#" title="subscribe my rss feed"><img src="./img/icon_rss.png" alt="rss" /></a>

		<a href="#" title="email friend"><img src="./img/icon_mail.png" alt="email" /></a>

	</div>

	

	<div class="clear">&nbsp;</div>

</div>

</div>



<!-- SLIDER -->

<div id="container_all">

<div class="container_fit" id="slider">

	<?php 

		if($m=="01"){ include("./01_home.php"); }

		elseif($m=="02"){ include("./02_about.php"); }

		elseif($m=="03"){ include("./03_portfolio.php"); }

		elseif($m=="04"){ include("./04_contact.php"); }



	?>

</div>

</div>





<div class="container_fit">

<div class="container_16">



	<?php if($m!=="02" OR $m!=="03"){ ?>

	<!-- LATEST PROJECT -->

	<div class="grid_16" id="latest_project">LATEST PROJECTS</div>

	<div class="clear">&nbsp;</div>

	<div class="grid_4 latest"><a href="./img/latest1.jpg" class="fancylink" title="San Antonio Website Design"><img src="./img/latest1.jpg" alt="" /></a></div>

	<div class="grid_4 latest"><a href="./img/latest2.jpg" class="fancylink" title="San Antonio Website Design"><img src="./img/latest2.jpg" alt="" /></a></div>

	<div class="grid_4 latest"><a href="./img/latest3.jpg" class="fancylink" title="San Antonio Website Design"><img src="./img/latest3.jpg" alt="" /></a></div>

	<div class="grid_4 latest"><a href="./img/latest4.jpg" class="fancylink" title="San Antonio Website Design"><img src="./img/latest4.jpg" alt="" /></a></div>

	<div class="clear">&nbsp;</div>

	<?php } ?>

	

	<!-- SUBCONTENT -->

	<div class="grid_16" id="subcontent"><br/><br/></div>

	<div class="clear">&nbsp;</div>

	<div class="grid_4 subcontent" id="subcontent1">

		<h1><?php get_title(86); ?></h1>

		<p><?php get_content(86); ?></p>

	</div>

	<div class="grid_4 subcontent" id="subcontent2">

		<h1><?php get_title(87); ?></h1>

		<p><?php get_content(87); ?></p>

	</div>

	<div class="grid_4 subcontent" id="subcontent3">

		<h1><?php get_title(88); ?></h1>

		<p><?php get_content(88); ?></p>
	</div>

	<div class="grid_4 subcontent" id="subcontent4">


		<h1><?php get_title(89); ?></h1>

		<p><?php get_content(89); ?></p>

	</div>

	<div class="clear">&nbsp;</div>

	

	<!-- FOOTER -->

	<div id="footer">

	<div class="grid_8">

		&copy; Copyright 2013. Austin Web Design. All Rights Reserved.<br/>

		Web by <a href="http://www.webdesign4austin.com" title="Web Design Austin" target="_blank">Austin PHP Developer</a>

	</div>

	<div class="grid_8" id="footer_menu">

		<a href="./index.html" title="HOME">HOME</a>

		<a href="./about.html" title="ABOUT US">ABOUT US</a>

		<a href="./about.html" title="SERVICES">SERVICES</a>

		<a href="./portfolio.html" title="PORTFOLIO">PORTFOLIO</a>

		<a href="./contact.html" title="CONTACT US">CONTACT US</a>

	</div>

	<div class="clear">&nbsp;</div>

	</div>

</div>

</div>

</body>

</html>